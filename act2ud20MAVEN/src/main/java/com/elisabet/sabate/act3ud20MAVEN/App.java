package com.elisabet.sabate.act3ud20MAVEN;

import java.awt.EventQueue;

import vistas.vistas;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					vistas window = new vistas();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
    }
}

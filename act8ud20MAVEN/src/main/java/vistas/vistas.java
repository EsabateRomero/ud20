package vistas;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class vistas extends JFrame{
	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField txtConversor;
	private double cantidadConvertir = 0;
	private double resultado = 0;
	private JButton btnNewButton_2;
	
	public vistas() {
	
		getContentPane().setBackground(new Color(232, 222, 210));
		setBounds(100, 100, 327, 293);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lblNewLabel = new JLabel("Cantidad a convertir:");
		lblNewLabel.setBackground(new Color(232, 222, 210));
		
		textField = new JTextField();
		textField.setBackground(new Color(163, 210, 202));
		textField.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Resultado: ");
		
		textField_1 = new JTextField();
		textField_1.setBackground(new Color(163, 210, 202));
		textField_1.setColumns(10);
		textField_1.setEditable(false);
		
		final JButton btnNewButton = new JButton("EUROS A PESETAS");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				cantidadConvertir = Double.parseDouble(textField.getText());
				if(btnNewButton.getText().equalsIgnoreCase("euros a pesetas")) {
					resultado = cantidadConvertir/166.386;
				} else {
					resultado = cantidadConvertir*166.386;
				}
				DecimalFormat df = new DecimalFormat("#.000");
				textField_1.setText(df.format(resultado));
			}
		});
		
		btnNewButton.setBackground(new Color(94,170,168));
		
		JButton btnNewButton_1 = new JButton("CAMBIO");
		btnNewButton_1.setBackground(new Color(94,170,168));
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(btnNewButton.getText().equalsIgnoreCase("euros a pesetas")) {
					btnNewButton.setText("PESETAS A EUROS");
				} else {
					btnNewButton.setText("EUROS A PESETAS");
				}
		
			}
		});
		txtConversor = new JTextField();
		txtConversor.setBackground(new Color(163, 210, 202));
		txtConversor.setFont(new Font("Trebuchet MS", Font.BOLD, 15));
		txtConversor.setEditable(false);
		txtConversor.setText("  CONVERSOR");
		txtConversor.setColumns(10);
		btnNewButton_2 = new JButton("BORRAR");
		btnNewButton_2.setBackground(new Color(94,170,168));
		btnNewButton_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
					cantidadConvertir = 0;
					resultado = 0;
					textField.setText("");
					textField_1.setText("");
			}
		});
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(33)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(txtConversor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_1)
								.addComponent(btnNewButton))
							.addPreferredGap(ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(textField_1)
								.addComponent(btnNewButton_1, Alignment.TRAILING)
								.addComponent(textField))))
					.addContainerGap(57, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(95)
					.addComponent(btnNewButton_2)
					.addContainerGap(127, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(21)
					.addComponent(txtConversor, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_1))
					.addGap(30)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNewButton)
						.addComponent(btnNewButton_1))
					.addPreferredGap(ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
					.addComponent(btnNewButton_2)
					.addContainerGap())
		);
		getContentPane().setLayout(groupLayout);
	}
}
